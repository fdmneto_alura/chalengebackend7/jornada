
## Montagem do ambiente  
npm i @nestjs/typeorm typeorm  
npm i reflect-metadata sqlite3  
npm i -D @swc/jest @swc/cli @swc/core  
  --> No package.json:  
      "tsc": "tsc",  
      "test": "npm run tsc -- --noEmit && jest",  
npm i -D supertest  


  
    
Incluir .env na raiz do projeto  
Incluir .env no .gitignore








## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Observações 
Para a tabela ficar com o nome da entidade no plural tem de colocá-la no plural em: @Entity({ name: 'depoimentos' })  
