import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";


@Entity({ name: 'depoimentos' })
export class DepoimentoEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ name: 'depoimento', length: 255, nullable: false })
    depoimento: string;

    @Column({ name: 'nome', length: 150, nullable: false })
    nome: string;

    @Column({ name: 'foto', nullable: false })
    foto: string;
}
