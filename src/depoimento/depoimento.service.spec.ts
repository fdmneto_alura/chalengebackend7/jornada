import { Test, TestingModule } from '@nestjs/testing';
import { DepoimentoService } from './depoimento.service';
import { DepoimentoEntity } from './depoimento.entity';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigModule, ConfigService } from "@nestjs/config";
import { Sqlite3ConfigService } from '../config/sqlite3.config.service';
import { AlterarDepoimentoDto } from './dto/alterar-depoimento.dto';

describe('DepoimentoService', () => {
  let service: DepoimentoService;
  let repo: Repository<DepoimentoEntity>;
  let configService: ConfigService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        // Esta forma também funciona
        // TypeOrmModule.forRoot({
        //   type: 'sqlite',
        //   database: 'dataSqlite3_jornada',
        //   entities: [__dirname + '/../**/*.entity.{js,ts}']
        // }),
        TypeOrmModule.forFeature([DepoimentoEntity]),
        ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forRootAsync({
          useClass: Sqlite3ConfigService,
          inject: [Sqlite3ConfigService],
        }),
      ],
      providers: [DepoimentoService],
    }).compile();

    service = module.get<DepoimentoService>(DepoimentoService);
    repo = module.get<Repository<DepoimentoEntity>>(getRepositoryToken(DepoimentoEntity));
  });

  it('should be defined', async () => {
    let lista = await service.listar();
  });

  it('Criar', async () => {
    const depoimento = new DepoimentoEntity();
    depoimento.depoimento = "A Jornada foi uma das melhores agências de viagens que eu já experimentei. O serviço ao cliente foi excepcional, e toda a equipe foi muito atenciosa e prestativa.";
    depoimento.nome = "LLauroo MMatoss";
    depoimento.foto = "iVBORw0KGgoAAAANSUhEUgAAADgAAAA4CAYAAACohjseAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAB6SSURBVHgBPXtpjKXpedX59rtX3arqW9VLdU/37NMz49knE9vYDpjEdqJIyY9IKBEgFARYRIB/IYRkRUj8ACUIofyIkEBISAhLQQhiUBzIZntiO56Znmlnenqf7lq71rvf+60553nvTJevq+rW/b7vfZ/lPOc5zzte+O++XpV5iqoq4AcB9M/zPP5eoSp8+JXPn4Gy5Pt5iYovv6zsPY9f4Pslr638Cn7oA/qf79v1ZeghqPHnmPfTe/ywHwfwkgIriYdWBNT5SN8rEfHCNMvgFQXSssCszDDIKkz0TD/gXwPUwhBFyGv4jCXeL/a5Aq4j5/Mnno8Z11pWJVdVoRZwPVxDWMxnXGHBbXHRWizX7PEPnt4pKhT6U85F6A/2nu0JFRfl6T1ttNQiPG7QQ8hNljRAwZevP+ummYyRmxEqL0Mc+ajx7XoRIOTnQt6L6+bLR8JNeFUGrWAWFjjJM2T2QK6h4kLmBZLKQ8JNJ2Ekb2DOe0y4hlk5Qx54XHOJAXfu657mOblDnuMf9LMnD3FnXrHwWiWPLrwL542A3uatzPO8CHUuLORHIlnUL+kF+osWzUtnNY8P5tYhJ7f58Brfa+TaWIXIC7VOey4sGjKLhBqvX/EjpDRSRY/6hbuX7wV8Do3J6+WMIsstxFIaI9VanXfMSaFvbjZTg2a3TVYWos76Hr1LA9vvconvyS4lWtx9hwas8bMJN1bT5ujBnNeMuIghQ2TKxQ8ZBZkuhYzgYyMpsZloU9y87skbB0Vmi5EtLOb0CF3hyQNAzPfkj0Kf9eEWbtFT0oj8W5rTEQXvw+9yd+lcIceEzRoXNa/AzdsNmJDwYt/CrbIH0kOFe0hIS9MRYPqgSQ9eCEt+Z57Jl/x7TksEQUKrBxjyhg/nGYZpYd4L+brIXFyPPNQtsgu3D/6L9H/0RulrDfKKb/uUW0u+QnpMQRswclA6QChpFKWCxR4/HPsFGvx9bhuGrdkvPKVAZTsWeECWYLgh4g24EN3PS33UGfervCam22MIGBj/XHAvaqFTb9KrEfMkht9c5e9dLtLH4ckx1upT7Az3sR/NMOdXorxQvpqTZEAZT6FicIWgdJvyF+AlsLDIKUrbl5am3BQulJ+8x6hSFAmIZKAJL5jSm2Xh9hXOuSnttJqnKHP+nAgsQkSEqoJ3UuYt+yHWch8N5mGDP4fMi6WoxzxYwugkx97hEdLZEHEwQi3eRWdjHcdHjwy8oqzAC2s9FEnOvZzAz4e0fGm5WSk0uUovirkv29an/0LGotJBqFgaNlhE8uVCTx/2+aYQX1/1SmDDaCoVAeXC0z5DfDDhg0PbsRK9EjCFzjKyVqDN03P5IoT8son5PnBr756VgtHJGKNH+ywxIeJmA0vr69i+8SGS7jK6Z9ew3KwTqhm+Rxlq+TKiMzGm6b4ZM9MzlWtCUgJL6IcG6ELoMPYtPZS78rJfGpDaugOGrEI0ZCT5oUP8OTcVM++VMjnXPzeQK7hBPgi5AxLlHnHe4Q3fL8uF9bjzgO/7Ew/9rSGO7u4gjCKc7h/QCwSV/UeI201UWQc7+zvcaJNry7C9t4u427YNrJ07i3ROr/QDtHoM4+jQSknOnGc2GehEQmVtmYsN8sq8KqBTZcu42ELrMTwszIM5r48UhkynhJtuEcKrOlOI100ZeoO5DEegKBzMMY5dblioFK4ONuiZBi8IRgUG7x5hfNTHZDAy9Jue9F1tJITn0wmO+icM0Rpm/QFOtrbRvXAO+eCUm2/gJJ2jttblq80ifYFhNGcRP8WQqFcyjFULlTSBkI+bajKvg8VaFM5jpouApgxUS12NrbEulTR8wF8iK1O+5WODnh+zhja5+/BSB9jnByYZrwhcaShSz8IkMmApsRolwHGG+x98hLhep0Eye5X5XDSChqAXJqnlSZoXliwek3766BGaqysYHUxQb88NvVudJvYfPuSiWOR7MSbpKcYqAVDJqdDiIlgVkRY0iBeawRVJExpxRm8XWiMfRELEtPGxHCfcuG/YEaoQ874NLtejIWoM37BVT4g8pEczuIIuNKVVA1qrxxhfi+lBIunOjT1exNo1n5u3y9mcG+LDUZidtZDIiIVvQJDL8rMphgzfqMa8m09R8iGnKifTMeJWDdU0RrkaoJ/NbBNz3r/gAlt8bmxhaVlnQJMxOeVJz3cxpgKhUpLl8j6fy/A0kGQOxgzXnF9e6EqMUaclmjDNYLVFyKSk7vKBXYIMplMMtveYh9w2b1LxplEUGAKKW9biCKvNAC8+voxep4F5WuLW1glubg8Ygrwp62GIBNVkhMHOfcz4e0wze2kLB80WISA3qie6lTFycqZNmwszT/HxM74/VX0mCHlcg1HeT2igI2HcfIEaLax6m5PqhdxDQUOHM7EiemyJ3hBLkGUCWq7GByyrzjAPjYNHort80WzLzQrPrrdxtlvHxkob585u4NKFi2hz0SE9oEJ8eHyEH177Cd6/s4eHx6yDROo+6+kxwzUgsk4HY0RFCu/55qeI7S9AZUDqNVeIGdmvMKZBVdYaUcZiTnjg2gohsO+7MiociOEozmLHSoFA3h1Pcyxxk+2SYcGLA9soEU2hIFrGkNXvTXpouD/CF1+6hC9cPY/HN9ZQS2poNtqo1zuIkrrdVIRU19WWzyHprGFz8y6OhkNu0MOHuwP84N4xZgxAYz31CDUCTMk0MNYiEHEMjABUYGpUUZtxdc+KMj+vmicjGm0k4bZa6DmSPyOqZ4UL5yKnB1XcIiZMvRS55SbF5sUWRGKLympdRiA5+8IFXFqd41d/+RfQJAIu1xJalGZj0Vd7FAaO2QcME48PjWjl7uo5glIb48kEx+MRDqsHWBuSGPCVLC8x3/hc8tIqYHtk9VClwHUz4qX+J92KDMfN5lzwWECmRkCYyLCtcbPtJLHSJniYMxpHaWqbjQJ1KyLXOYxw+wIAoSM/rW4g891G9ZUstfBs9yLiOEY1m9l7TEor8AFzoxLzEDIGsYVOReTNY4YIw43PR4uLWq7HeOPxVWyPctw8JYvhJvNwSmRkmZFBxWYEdMbYFE4q9r5j+qXzUOm6L7UeVtjHBMi2Uqk0SoApn3c64f34vDbreqjeSWg1IcgIjQLPIVTJRQ7lclo14YMqZnaD4ZcTKJKIkNFoIoqbLvA9OIsajyQ0hzEjxzpf1i2FkXu9dfUpvHf7JsbZEKtsCCfsDVOisD6j9qwUVVK4qd7Yd2OsLqKyAos2w55lIU0HzPgaq36WhdVNgUuT60xFHhRZAyJeVKq/KrHMMFO+lfyw2EdWOojOacWU7j7pH2M8XsHK+pqKEEO3bwUi588RbzrLppgStd75+Aj3Hx2j121g48wqNs9vImx0CGYJXnnuJezO3kFjfoKyQcbB5/VnuVKd7MezVs2zrsQzwDFlwOL1k/1Zs2eNgDqNGYv6cTEjoMRoMzXqUgn4vSxd6Qq1S9USbUIW0EuNqtoZkdYkCF1vRUuVCW+wcg43HuwgHR4TynMsh5k5SPmjTf3v69v4+HCG9fUu1laW0bi/g+xHNzGMl/DCi0/h2Iuxv/I8U5fEe3DAmJqwprquolywKY9Fu6DhVawXXZMBidA8sN6TuOE51hWbf0urgVpjLVaaEFrK0EqH2I2FVkQgKQXbzCFRpZjv9ehReyg/xGVg0lzC7/3RD/DR7bs4PT1hDgTYXPbx2qUumq1l/On1XUwmuVGk4aiPY4Zzh9f1BCg04CuvkKv2M0RTbqh/gCtNH/+w1cYfjjP8zt6pRY3PfPVzpbfCjWWAHgoIfitsItvEtCjwHGOVbKGdC2VL3xnAk+pTOoAyF3ODCW/aMP5XLTr3wsKyQTfXAsfvpAhc/+gIuxseXvDnOMusnkxZUCdT7PVD/O47fXqxj9fON/HMmQT7J0OG5zKabPUbzRo2L2ziHgt/i8DzzMVVrBNV6z0umCF+fmMF5x48wB+nMQ7JV+vM73YinmlJTQNLbGKD3YDpPZWRbHUVngqi9YZ5tiAn3kIdM10mN+QN26wU4oBRqbj3zStqYFUCEkG/EDVIkWR93ryF+9vbaHHzVzc6dsNEMf/xKX7jl97Ct9+5hy+/cZW52mfkDVkffTz/7ItY2XgcwQ/fxcWLF2nwGPXTY0x2d7B76yauPP4rOHehgZ9b2sL7R+w+WHaUFL7FoEPxKPGN1cirUtwqbqzMyk/BLUkWzbk8Zwjkyoi8HTZ4g5bpHlS0uLkmEVANr2jYjIUlsA2HOP9YD81uDc/Gm0hPjjCbjrDeW8VTl86jHTzAF/7aV9HtfYjd4wE+9+LTqDHbQ3rD7/Twzk9u47XPvIC4VieNSy1nn736Om4e7WHrxl/g8qs/g2C6hZVWwudFJlVK5ijUIikg1f95lWuXcs/hjefKQhwu0McUMs+iz8ljjtGEokJN2kyFuy61zGgQiW/haolYhTZc79TZsFZ48zPPYYNiplBTVvzO23+JX/nKX0eru4GXngpQv3MH//UP3zOts04i2fZTvPHyk3xGAwU7jkxKGI13uH0Xl198gw3zKd4fbmM3HfGeMYu402UUi9pgxi6i8nKjZKIyCwHQWiVpHEJb11L5hpwFtaDAc0xMmwx5LcMisJzzF73XKJtjRjFYFEnc0iP5FRDlDJmDAROZjGFrZxvvXruOv/0LfxM+c+fWn30bp2xo28tn8NTyHDt7WyiHE7TP9bC3fc9Qud5esg1IZkqaXZKHJcxrLfze/b/AmJ6dlY5UOOWuMsaiFss2R6+GXmVpJEIdcT2x1syNSrpQTmapjOE7RFb+iZgLLU3CE8zya5qlmEtiU3FVrZNsoKQgZK93EqzPH2K4t4fTe1vo0AB/+t0/wvb+IdaIDLuUPxqtJr74+Z/G0wSP/ft3uKDYELU5ODHKNeG9y7Bl4TaoT/A/Hn1E9W28IM+OB6vlMXVdjYwK/EIuLBfcVI2n8CRc5FmmsJSkyQ7HaKrvaqfeC+u0jDZUka9Z28Kb5dIYmXuyVmXcrzQivMu26UN6LqF341oDm70VSvNddKiozQm1m+d7uHDxSTKcVYZbSU0mw+nhLjsOeo694Hw+YUPMTqGsYWc0wncm1zHgveQZIaTCjVHqmmFprMy3OQUWPV9OTBWCGayceAShKjC93LjOhFGnsmJg47uuROUkVEtRai4gxiBtxnfKaiHBV8mtUFH7orZlWmKHLU/AvNlY35CUgw8/esSiPMOVC6voEXRmk1PsHwxxe2sfN288wFvPrWKl06Kl2SGMx5TWqdhxXlFb6vA6NbYk5lbQC7dBbdSYAzcaOT010AYXOmfJ3FaNCxJXDkZUFebMaznJqJmJ056xsLTQBj2nbAulvDi00KCEyQ0X1nzOKke656ZjltimoPQyZwAjdusf3t3nRob4t7/9G3jizS+RRbQIZjlGR9vYuPYRrj5+E8dbH5nFZdGcOZKzNiZS08lVtWBDTIN2h/PylJgVrGi7wYoUc5/SuboetUA2Pwl989CcjjiazKgYzFiyyJf5OYW5uooxNx0KYOQ1eVGWUW4UFGzGfLh6hpQ3sq7CM7fiXtTGT6mBJH+8sNHCyuoGyn4Tox895IPYzMYNHI1PkFBvPd8jZx0+ouczo4KlJ4k+RJc1tpDEUFSfaqFqk1IWbKlnMy91kiEb1ISvKgpsKBPROFHBXpI7nM8JhsUE/XRis5AaJYlQ8eaVTg2v8Z5DblCdgascTpJTjya6Nssk9BQm6IgAq+AqJPa4kvvszs/RfmcYkkljDW9//0c4w6Y3y6dkFSmRNMaFza5pOCsUffuDAcaz1ITZJdKSiLUuF3xnCkxamhEzIvecUMootKGE4cbuPSFn81vMUQ0+wrrjxAzHYiZj0csZGVGYk/nkxlslIyrsy9J1OAFTi9op/8gPZ5l6/9D+4ERhz6yumpSbuCRql9sQ5HucP/w0haMzBKZOmxb3j/Hu3fcxYydx9bGzWF97Ep1G3TaYMT/FQ2d86ISbvEIFwFi+9XmehVyfuf2AfDQjF66x+Y25nnbHs+FhmbDfpOfU82ptWUiPUS2r2G7FRWUExReBVQVQOjHysoXkWaMbw3Dp0HSxPAvIMiK4SR0TngWr5buOWnm6x1Z6RIZeYzhsc21vU4v5PDvzNdaiSxcu4OlLF2xKFFPGqJE4yjNTuubW1gGO+0NudIoLbJ3q5KMBmVGvdRaD/oiRwkjYqHB39AFTg2lBGTNZIoiwlVKI+rFv+DBbqHdWAphCyuGoShYDTTf8KdLKBCqpaaJ2miiFSXfAsGQt1IxPLIIXjEYBeplvypXyskautz6p2AnUGfdMYnp6wO78XYZbeXqAyykLNnvF0XBg/LEKagSoGvpDgtGQDTLDZ3OtifPLbQJAZaxf5enza2fxH//nNfzzf/0vsfn42/jvf/6f0aF9O0TZJFE9pJxJo02cfmGRRvmcyFuaHBh41ESpNo0YfePS9Y5q+EQChMZJg81CovGZX1qBVCdto+lqQVbj0j4YcdFDU63YNs0CTUWppQT2/q1Ogd0PdnD64IQlYsZ8cnpITBahhV5YbeDpc6u4fHYVXSrckhnULHt8vXlxBZf+xa+judHD3zj/NRyQj147/H+0fuFqIPczEujQe0oODUsT3/WDbmTHsHepbGmkr8grrNWL1TSXFo+L8bXN5J3m0WAXEEWOxOq7ZL2UodTOR1juOAW5ob8z1PKU7OaJNfTo+Q9vP6QGMrWHn2fz9rhtrIceJfsOGY7019Ckfib/7JS1lfz3uReYgx5OBj7+1uv/CK8dkZ9mj/D97T/GtVOWmdwShpsgmKjZXRRzNQBi7ZVVADcFU7mZMLoERx2/simYtHFHjzTiCALHBU0yhLUls9SxG4FMu80mkwW2Fs0tj/K8jqj9KmH/CsqDb6P5zAYmJ4cmq6+0GuitdtFdbhLqGSoa6hCB1adpMFNOBzjofhbHAw+3d8d46XID9/f5fvoiHlvy8MprP4ufHP4Av/Pef8Ht8R3rC4dsfAdlZWGuUFSBr9jK+UFllE0b1MSqLrRlaSms8PuO85ULXVFfeeYmSza1XhiAIwkNnqiqkTQHYjwt/uHrVLC+hFvD93GRaNmgfNbtLds4O6Z8GJveSfhnb6jONOjwGSTkolyP4vPY676G928P8JnHOtg7mqNVq6Fdg6Hrzj7raPI6fvfLn8P147fx7z/gRgfb7EjYLcxp9MxNnVQeSq5nIsdIk6URh9JziD/L/D14+RfXv2mzBHE3O1XhktVaDZ2GYCcd02sxC7OKbo1h0Wl/Hkn9N/Hju5dxQvlheHSI9Z3vE/JJDUSZNB/g57RBJ7Hrvplx3bxQPq/j3hO/Sm4bkKfWrC6pea6k0dI4p2MdVqjM8yo9Zxvn8UuP/4yRgdu79zBnuSHFslc1ZIoR2zwOvDwdGLFsFVnJuQYdI8k9o+VShn24OT0WYRoTPQU0UeiYu18xl5r/gMj28/jo4QCD2chyI7vzAQasiy1xR35uNFdTSyCYZqa66V6MCRLtGby1x3D89N/FVurmhnr+3dMZHlutq7VhPXb3iBdk3y8DO70RUGX4py//Gn7x8ufwjf/7W/j44D4Nl7kxnw4OMGwqisi+sCHU+ufWCQVP/uyVb2bmwcrQ0Tc1XBAtgKlsYhTwgka0iW7n31BveRX3DkboT+bGcjLmaHbjT1A/uoEWkbYWe4auDkkjp2Eqwxl2ab2Hg1e/gVGth2POG9fIeIaEwY1ubCRZirZJDgvdU5EVOlXfJl+SMc60VvDlJz+L29MDbE0efTKQsOGt8Vt9XqRdk1GVpLNfeOqbGWtSmnOexzFZwA48tAM9lSWvBqRJuEQB6T/gzm4P+2xip5r+8qYT8U12+kc//jaC0485YQrN24oAa0zlCfLORr2Bqn0OW6/8ExxHG9aVJKyhhRvXGnOqRW5qNBq7EqU3pVi64abbh545L6Se1fGF82+SJ1e4uXvbOn/bZmAnjgylS0ax2Fk4mob2vspBELGwEhnrlO+biVgJ388Jte1/hj+7TUQtj4zviY2E3PxXn29jb2eMH9/5MYWoGY3SQauZcFBWmnHqCcXYdhuN3hO48dTfxzQ+y0lc7gCIobLLCdN5jgTKxexvTgMP2XHoSJmGL2kVEW/EXRkpzLeE4Vyvu3HSlH3iF7tfw/GzA3zn2v9hq2ezP5YN9zKva5iacrfUudk5i7H4NgOczSmH06J18r1W50W8d2uNF1GJJntI+cAzolHcwNajFP//P/0Wc62P9lrIkOMobZUzWg5nVA5azQ6Wz1zC9ku/zg2sIKFZW/TmlCh473SCs/zs6TSl0MVU4uYOxxMTm+eMmj4lyf6YODIn8SevrXOIquMEDBrDhYcHU3q2g1++8mu4++Aubm2/Z+DikaB7NTeh1hwztGNPrvNiC0KYJQeVPKCfpzOxiTdw/fZtPHP5SQzVjRNUNvjAdDTH//r+9zD7+B6eWGnipSdX8eYLz9HzXAjHzwU7bOVNQSV71jyHVc4yhLhbHw9xSi/FyhOpBTRwg6M3iVxzdvdt5tksLxcCrhhNyFo5Q0TBqslBa5NxOyVtbHAWyeaDfDfC33vr6/jN//YNzCiNyGsW4rEbywXnvnTlm+KGRu6tf3fhop8yvr939DKO96bodNdMvlOd7JFyBbzYJlGDR/i5Jzx89uWrWCZbWWJIRkliU6Y4rtnJqKN5B/eyLt67NzBxqUEmsEJBWKi9thRbvR0QtNTn+p7SxLVOEoEl693bPzUhKuZ9p3RAlyxpamcIPUut6YipwPv85MN3iJOG23ZSUr2fb4fvFpMhG1wt2iWNs7OMeghzULr/hDN5wE10ECwO3yQtXLr6U7i6eQ5nNy+bXFfYlEgAwdzVqI2q2ezoNn54d4DD/gx7VL2bzEE1rOq+cy7ipD+1UxKSGnW+5Xg8NclEAPTgsM9eckavFtYUiK3sHMxw1E+tRkqoUk5/9vLP4/KF51DRADq1AaUedV3fyQWVTXRkPS0y8J3YFNg5C3cST1YekeGkOuklT3IhtS6lv81naUXO2cnq60vLYu+kc+rDIqS1ZS6Uwxas4uade3zwGCsNd6pJMoSEKsn8kicmE3nWt43pgE+d6pPapGMmojweEo23WZ5OGa6D6dyiTJxYJKJJkbk/yPB3vvKP7URixTat1DTUzowK8UzIgVnD5hmVOy7lew16qeNG2fxAg4uLWWimuTv7ecLv62ttpFTR4pSIwB6vWt5ESTV74HHsIgmDuf3DwSpW28wzdvviqBKxROeOh1NT02XkmOEoFUH5Zwo2XwenU6oE7OyZew+omCfMT+k10rQV+grX8aw0BNFBpTrO4vlXXmdJItiQgQXNVCehHE0KfFmuNM98MkhstdYwosWkbPcztSySHGok4AXBpGZnUq7tPMIWqL0QIYujLUz272N+uI9GNqD/K7zvXcYH12/heD6ysjGc56YS6PSFSEGd9XBCcJnqlG/uTjZqLqJQvbdLbYfFUfN9O3uaYTFi95mzU9ND3RlX3of5fkSDvPXaV+Gtc8sbROwNU9jcydjAVOPQZAbfpDs2szmTeji2ENUpo+G8sOJ8yAwX/Wpw8uLVOng7Z/GeWLzzPYYocy/kHOKofQW//d0Jljh0qdEY7WbNSIQEEIVexOepf9RR5dKmWp5JlWrNdNRkyAbaZ8szJDFoMUxTIvhUZ2qI0l0KzTYRY3SpoIvlDGj4XuNZ1HoxlrucXTYyhahaI6GoI7is07SgZ6Ra1K3gjM/6RJ0Z9d30tcYwlSSxRtSUPHGdElT62Kt20iIkCdAhhP3aFfyrm08gbfawTcmiy3zVgLLBUBwSsFqNmvWZCrlpUdpIbKC88eTJOVGSpYa/6++GGQzJ4TR1ZwL4Uph7ll4sXfy8dNPVJaoIBz5evfIGRTB2QCwVkiZNZrD5mqRzkVzpkDbD4kCE7KacK0wjA5hPuo0RZUOdGbWj3AyPb2WvAi9+Gfnl1zB57mv4Vvo6rt34GClhvN1scb7XMKA6mYytHAnMTilpZIsjI5rGqiwUBn5U0Q8H3NAYh5T+dseppcMZ0sIZ2zLps+4MYWUvRZjv61BCzPtneHnpq6y/jk76lbdQzGx26JIbi0PpU5LZeltK7NSQtVDzq3BmGA8I3SPm0ZCtizz7B3f6+EG6ifvdV/C98Tn8/h98F52lBg76E5zrnbODC5pc6Z9AY8Q6pONxfXppRPqmUFVF1DFJdQlHHG3vn/RxSIl/QpFZxGAi2A8THBAlJTwHOhWVFWbw+cwNTyc0Tjm5wmnwJjfJ4UxBNS1nrhVlZKNg9WRl4cZUsnSrq8PpMzsvqn5MALEUh7YQoZiaXOXs7vEUv39tnw8o8K3vvEuZfsBFTtHorBKsaraxTyc+6scDJ0kK1KacTolvzugWTbXqrGuaZWjafHx8aMbI88yIwGg8Y+gxnwlYUuSDoDKSro4l1H+ywJ9PTkI8c/4rmPI5fwWe83XGFv4ELwAAAABJRU5ErkJggg==";
    
    const depoimentoCriado = await service.criar(depoimento);

    let serviceFind = await service.listar();
    expect(depoimento).toStrictEqual(depoimentoCriado);
  })

  it('Listar', async () => {
    let repoFind = await repo.findOne({ where: { nome:'LLauroo MMatoss' }})
    let serviceFind = await service.listar();
    expect(repoFind).toStrictEqual(serviceFind[0]);
  })

  it('ListarPorId', async () => {
    let repoFind = await repo.findOne({ where: { nome:'LLauroo MMatoss' }})
    let serviceFind = await service.buscarPorId(repoFind.id);
    expect(repoFind.id).toBe(serviceFind.id);
  })

  it('Alterar', async () => {
    let repoFind = await repo.findOne({ where: { nome:'LLauroo MMatoss' }})

    let id = repoFind.id;
    let novosDados = new AlterarDepoimentoDto();
    novosDados.depoimento = "depoimento alterado";
    novosDados.foto = "---";
    novosDados.nome = "Novo nome";

    await service.alterar(id, novosDados);

    let repoAlterado = await repo.findOne({ where: { id:id }})
    let ehIgual = true;
    Object.entries(repoAlterado).forEach(([chave, valor]) => {
      if (chave !== 'id') {
        if(novosDados[chave] !== valor){
          ehIgual = false;
        }
      }
    });

    expect(ehIgual).toBe(true);
  })

  it('Excluir', async () => {
    let repoFind = await repo.findOne({ where: { nome:'Novo nome' }})
    
    await service.excluir(repoFind.id);

    let repoAlterado = await repo.findOne({ where: { id:repoFind.id }})
    
    expect(repoAlterado).toBe(null);
  })
});
