import { Controller, Get, Post, Body, Param, Delete, Put } from '@nestjs/common';
import { DepoimentoService } from './depoimento.service';
import { CriarDepoimentoDto } from './dto/criar-depoimento.dto';
import { AlterarDepoimentoDto } from './dto/alterar-depoimento.dto';
import { DepoimentoEntity } from './depoimento.entity';

@Controller('/depoimentos')
export class DepoimentoController {
  constructor(
    private depoimentoService: DepoimentoService
  ) {}

  @Get()
  async listar() {
    return await this.depoimentoService.listar();
  }

  @Post()
  async criar(@Body() criarDepoimentoDto: CriarDepoimentoDto) {
    const depoimento = new DepoimentoEntity();
    depoimento.depoimento = criarDepoimentoDto.depoimento;
    depoimento.nome = criarDepoimentoDto.nome;
    depoimento.foto = criarDepoimentoDto.foto;
    return await this.depoimentoService.criar(depoimento);
  }
  
  @Get(':id')
  async buscarPorId(@Param('id') id: string) {
    return await this.depoimentoService.buscarPorId(id);
  }

  @Put(':id')
  async alterar(@Param('id') id: string, @Body() alterarDepoimentoDto: AlterarDepoimentoDto) {
    const depoimentoAlterado = await this.depoimentoService.alterar(id, alterarDepoimentoDto);
    return {
      depoimento: depoimentoAlterado,
      mensagem: 'Depoimento alterado com sucesso',
    };
  }

  @Delete(':id')
  async excluir(@Param('id') id: string) {
    const depoimentoExcluido = await this.depoimentoService.buscarPorId(id);
    await this.depoimentoService.excluir(id);
    return {
      depoimento: depoimentoExcluido,
      mensagem: 'Depoimento excluído com sucesso',
    };
  }
}
