import { Module } from '@nestjs/common';
import { DepoimentoService } from './depoimento.service';
import { DepoimentoController } from './depoimento.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DepoimentoEntity } from './depoimento.entity';
import { DepoimentoHomeController } from './depoimento-home.controller';

@Module({
  imports: [TypeOrmModule.forFeature([DepoimentoEntity])],
  controllers: [
    DepoimentoController,
    DepoimentoHomeController
  ],
  providers: [DepoimentoService],
})
export class DepoimentoModule {}
