import { Test, TestingModule } from '@nestjs/testing';
import { DepoimentoHomeController } from './depoimento-home.controller';
import { DepoimentoService } from './depoimento.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DepoimentoEntity } from './depoimento.entity';
import { Repository } from 'typeorm';
import { AlterarDepoimentoDto } from './dto/alterar-depoimento.dto';


describe('DepoimentoController', () => {
  let controller: DepoimentoHomeController;
  let service: DepoimentoService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DepoimentoHomeController],
      providers: [DepoimentoService,
        {
          provide: getRepositoryToken(DepoimentoEntity),
          useClass: DepoimentoEntity,
        }],
    }).compile();

    controller = module.get<DepoimentoHomeController>(DepoimentoHomeController);
    service = module.get<DepoimentoService>(DepoimentoService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('listar 3 depoimentos aleatórios', async () => {
    let depoimentoEntity1 = new DepoimentoEntity();
    depoimentoEntity1.nome = "Nome mock 1"
    depoimentoEntity1.depoimento = "Depoimento mock 1";
    depoimentoEntity1.foto = "1111111111111111";

    let depoimentoEntity2 = new DepoimentoEntity();
    depoimentoEntity2.nome = "Nome mock 2"
    depoimentoEntity2.depoimento = "Depoimento mock 2";
    depoimentoEntity2.foto = "22222222222222222";

    let depoimentoEntity3 = new DepoimentoEntity();
    depoimentoEntity3.nome = "Nome mock 3"
    depoimentoEntity3.depoimento = "Depoimento mock 3";
    depoimentoEntity3.foto = "3333333333333333333";

    let depoimentoEntity4 = new DepoimentoEntity();
    depoimentoEntity4.nome = "Nome mock 4"
    depoimentoEntity4.depoimento = "Depoimento mock 4";
    depoimentoEntity4.foto = "444444444444444444";

    const listaAleatoriaMock = [depoimentoEntity1, depoimentoEntity2, depoimentoEntity4];

    jest.spyOn(service, 'listarDepoimentosAleatorios').mockImplementation(async () => listaAleatoriaMock);

    const listaController = await controller.listarDepoimentosAleatorios(3);

    expect(listaAleatoriaMock).toEqual(
        expect.arrayContaining(listaController)
    );
  })
});
