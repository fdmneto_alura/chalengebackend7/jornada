import { Controller, Get } from '@nestjs/common';
import { DepoimentoService } from './depoimento.service';
import { DepoimentoEntity } from './depoimento.entity';

@Controller('/depoimentos-home')
export class DepoimentoHomeController {
  constructor(
    private depoimentoService: DepoimentoService
  ) {}

  @Get()
  async listarDepoimentosAleatorios(quantidadeDepoimentos: number = 3) {
    
    return await this.depoimentoService.listarDepoimentosAleatorios(quantidadeDepoimentos);
  }
}
