import { PartialType } from '@nestjs/mapped-types';
import { CriarDepoimentoDto } from './criar-depoimento.dto';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class AlterarDepoimentoDto extends PartialType(CriarDepoimentoDto) {
  @IsNotEmpty({ message: 'O nome não pode ser vazio' })
  @IsOptional()
  nome: string;

  @IsNotEmpty({ message: 'O depoimento não pode ser vazio' })
  @IsOptional()
  depoimento: string;

  @IsNotEmpty({ message: 'A foto não pode ser vazia' })
  @IsOptional()
  foto: string;
}
