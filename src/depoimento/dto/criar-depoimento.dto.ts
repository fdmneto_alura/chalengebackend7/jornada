import { IsNotEmpty } from "class-validator";

export class CriarDepoimentoDto {
    @IsNotEmpty({
        message: 'O nome não pode ser vazio',
    })
    nome: string;
    
    @IsNotEmpty({
        message: 'O depoimento não pode ser vazio',
    })
    depoimento: string;

    @IsNotEmpty({
        message: 'A foto não pode ser vazia',
    })
    foto: string;
}
