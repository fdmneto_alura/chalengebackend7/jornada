import { Test, TestingModule } from '@nestjs/testing';
import { DepoimentoController } from './depoimento.controller';
import { DepoimentoService } from './depoimento.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { DepoimentoEntity } from './depoimento.entity';
import { Repository } from 'typeorm';
import { AlterarDepoimentoDto } from './dto/alterar-depoimento.dto';

describe('DepoimentoController', () => {
  let controller: DepoimentoController;
  let service: DepoimentoService;
  let myDepoimentoService: DepoimentoService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DepoimentoController],
      providers: [DepoimentoService,
        {
          provide: getRepositoryToken(DepoimentoEntity),
          useClass: DepoimentoEntity,
        }],
    }).compile();

    controller = module.get<DepoimentoController>(DepoimentoController);
    service = module.get<DepoimentoService>(DepoimentoService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('listar', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const listaMock = [depoimentoEntity];

    jest.spyOn(service, 'listar').mockImplementation(async () => listaMock);

    const listaController = await controller.listar();

    expect(listaController).toBe(listaMock);
  })

  it('buscarPorId', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const buscaMock = depoimentoEntity;

    jest.spyOn(service, 'buscarPorId').mockImplementation(async () => buscaMock);

    let buscaController = await controller.buscarPorId(depoimentoEntity.id)
    expect(buscaController).toBe(buscaMock);

    jest.spyOn(service, 'buscarPorId').mockImplementation(async () => null);
    buscaController = await controller.buscarPorId('aaaaaaaa')
    expect(buscaController).toBe(null);
  })

  it('criar', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const criarMock = depoimentoEntity;
    jest.spyOn(service, 'criar').mockImplementation(async () => criarMock);

    let criarController = await controller.criar(depoimentoEntity);
    expect(criarController).toBe(criarMock);
  })
  
  it('deletar', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const excluirMock = depoimentoEntity;
    jest.spyOn(service, 'excluir').mockImplementation(async () => {});

    await controller.excluir(depoimentoEntity.id);
    expect(service.excluir).toHaveBeenCalled();
  })
  
  it('alterar', async () => {
    let depoimentoDto = new AlterarDepoimentoDto();
    depoimentoDto.nome = "Nome dto"
    depoimentoDto.depoimento = "Depoimento dto";
    depoimentoDto.foto = "123456-";

    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const alterarMock = depoimentoEntity;
    jest.spyOn(service, 'alterar').mockImplementation(async () => alterarMock);

    const retornoController = await controller.alterar(depoimentoEntity.id,depoimentoDto);
    expect(service.alterar).toHaveBeenCalled();
    expect(retornoController.depoimento).toStrictEqual(alterarMock);
  })
});
