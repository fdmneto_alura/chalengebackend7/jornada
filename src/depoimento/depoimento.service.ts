import { Body, Injectable } from '@nestjs/common';
import { CriarDepoimentoDto } from './dto/criar-depoimento.dto';
import { AlterarDepoimentoDto } from './dto/alterar-depoimento.dto';
import { DepoimentoEntity } from './depoimento.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DepoimentoService {
  constructor(
    @InjectRepository(DepoimentoEntity)
    private readonly depoimentoRepository: Repository<DepoimentoEntity>
  ){}

  async listar() {
    return await this.depoimentoRepository.find();
  }

  async listarDepoimentosAleatorios(quantidadeDepoimentos: number) {
    const quantidadeTotalDepoimentos = await this.depoimentoRepository.count();
    if (quantidadeDepoimentos > quantidadeTotalDepoimentos){
      quantidadeDepoimentos = quantidadeTotalDepoimentos;
    }

    const randomDepoimentos = await this.depoimentoRepository
      .createQueryBuilder('depoimento')
      .select()
      .orderBy('RANDOM()')
      .take(quantidadeDepoimentos)
      .getMany();

    return randomDepoimentos;
  }
  async buscarPorId(id: string) {
    return await this.depoimentoRepository.findOne({ where: {id: id}});
  }

  async criar(depoimento: DepoimentoEntity) {
    return await this.depoimentoRepository.save(depoimento);
  }

  async alterar(id: string, dadosDeAlteracao: Partial<DepoimentoEntity>) {
    let depoimento = await this.buscarPorId(id);
    if(depoimento){
      Object.entries(dadosDeAlteracao).forEach(([chave, valor]) => {
          if (chave !== 'id') {
            depoimento[chave] = valor;
          }
      });
      return await this.depoimentoRepository.save(depoimento);
    } else {
      return null;
    }  
  }

  async excluir(id: string) {
    await this.depoimentoRepository.delete(id);
  }
}
