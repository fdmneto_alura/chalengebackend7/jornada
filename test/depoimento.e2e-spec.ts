import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { DepoimentoModule } from '../src/depoimento/depoimento.module';
import { DepoimentoEntity } from '../src/depoimento/depoimento.entity';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Sqlite3ConfigService } from '../src/config/sqlite3.config.service';
import { Repository } from 'typeorm';

describe('Depoimento Controller (e2e)', () => {
  let app: INestApplication;
  let repo: Repository<DepoimentoEntity>;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        DepoimentoModule,
        TypeOrmModule.forFeature([DepoimentoEntity]),
        ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forRootAsync({
          useClass: Sqlite3ConfigService,
          inject: [Sqlite3ConfigService],
        }),
    ],
    }).compile();

    repo = moduleFixture.get<Repository<DepoimentoEntity>>(getRepositoryToken(DepoimentoEntity));
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/depoimentos (GET)', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const depoimentoSalvo = await repo.save(depoimentoEntity);

    const response = await request(app.getHttpServer()).get('/depoimentos');
    expect(response.status).toEqual(200);
    expect(Array.isArray(response.body)).toBeTruthy();
    expect(response.body.length).toEqual(1);
    expect(response.body[0]).toEqual(depoimentoSalvo);
  });

  it('/depoimentos/:id (GET/:id)', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const depoimentoSalvo = await repo.save(depoimentoEntity);

    const response = await request(app.getHttpServer()).get('/depoimentos/' + depoimentoSalvo.id);
    expect(response.status).toEqual(200);
    expect(response.body).toEqual(depoimentoSalvo);
  });

  it('/depoimentos (POST)', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const depoimentoSalvo = await repo.save(depoimentoEntity);

    const response = await request(app.getHttpServer()).post('/depoimentos').send(
        {
          "nome": depoimentoEntity.nome,
          "depoimento": depoimentoEntity.depoimento,
          "foto": depoimentoEntity.foto
        }
      );
    expect(response.status).toEqual(201);
    expect(response.body).toBeDefined();
    expect(response.body.id).not.toEqual(null);
    expect(response.body.nome).toEqual(depoimentoEntity.nome);
    expect(response.body.depoimento).toEqual(depoimentoEntity.depoimento);
    expect(response.body.foto).toEqual(depoimentoEntity.foto);
  });

  it('/depoimentos/:id (PUT)', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const depoimentoSalvo = await repo.save(depoimentoEntity);

    let novoNome = 'Novo nome';
    let novoDepoimento = 'Novo depoimento';
    let novaFoto = '12356789-123456789-';
    const response = await request(app.getHttpServer()).put('/depoimentos/' + depoimentoSalvo.id).send(
        {
            "nome": novoNome,
            "depoimento": novoDepoimento,
            "foto": novaFoto
        }
    );
    expect(response.status).toEqual(200);
    expect(response.body).toBeDefined();
    expect(response.body.mensagem).toEqual('Depoimento alterado com sucesso');
    expect(response.body.depoimento.id).toEqual(depoimentoSalvo.id);
    expect(response.body.depoimento.nome).toEqual(novoNome);
    expect(response.body.depoimento.depoimento).toEqual(novoDepoimento);
    expect(response.body.depoimento.foto).toEqual(novaFoto);
  });

  it('/depoimentos (DELETE)', async () => {
    let depoimentoEntity = new DepoimentoEntity();
    depoimentoEntity.nome = "Nome mock 1"
    depoimentoEntity.depoimento = "Depoimento mock 1";
    depoimentoEntity.foto = "asdasd afwsadf aadasda asd arq";
    const depoimentoSalvo = await repo.save(depoimentoEntity);

    const response = await request(app.getHttpServer()).delete('/depoimentos/' + depoimentoSalvo.id);
    expect(response.status).toEqual(200);
    expect(response.body).toBeDefined();
    expect(response.body.mensagem).toEqual('Depoimento excluído com sucesso');
    expect(response.body.depoimento).toEqual(depoimentoSalvo);
  });
});
